import cgi

data = cgi.FieldStorage()

name = data.getvalue("key1")
meal = data.getvalue("meal")

print("Content-Type: text/html")
print()
print("<!DOCTYPE HTML>")
print("<html lang=”en”>")
print("<head>")
print("<meta charset=”UTF-8”>")
print("<title>Hello World!</title>")
print("</head>")
print("<body>")
print("<h1>Hello World!</h1>")
print("<p>hello, ", name, "</p>")
print("<p>User chose:, ", meal, "</p>")
print("</body>")
print("</html>")

