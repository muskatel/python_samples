# configure
import socket

HOST = '127.0.0.1'
PORT = 4200 # 1024 - 65535

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
	# connect
	sock.connect((HOST,PORT))
	sock.send(b'CONNECTED')

	user_input = ""
	while user_input != "!exit":
		user_input = input("Send: ")
		if user_input != "!exit":
			sock.send(bytes("{}".format(user_input), 'utf-8'))
		else:
			sock.close()



# do something on connection
# send number 42