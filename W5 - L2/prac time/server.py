import socket
import selectors
import types

HOST = '127.0.0.1'
PORT = 4200 # 1024 - 65535

clients = []

def RegisterClient(key, mask):
	sock_client = key.fileobj
	connection, address = sock_client.accept()
	print("USER JOIN:", address)
	connection.setblocking(False)
	events = selectors.EVENT_READ | selectors.EVENT_WRITE
	selector.register(
		connection,
		events,
		data = types.SimpleNamespace(addr=address))
	
	# store client as connected
	clients.append(address)

def HandleMessage(key, mask):
	sock_client = key.fileobj
	addr_client = key.data

	if mask & selectors.EVENT_READ:
		inc_data = sock_client.recv(1024)
		if(inc_data):
			print(inc_data)
			SendMessage(inc_data, addr_client.addr)

		else:
			print("USER LEFT:",addr_client.addr)
			selector.unregister(sock_client)
			sock_client.close()
			clients.remove(addr_client.addr)

def SendMessage(message, source):
	print("Source:",source)
	print("All clients:",clients)
	for client in clients:
		if client != source:
			print("Send to :", client)
			# find client in selector and call send()




sock_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock_server.bind((HOST, PORT))
sock_server.listen()
sock_server.setblocking(False)
print("Started server on ",HOST,PORT)

selector = selectors.DefaultSelector()
selector.register(sock_server, selectors.EVENT_READ, data=None)
print("Started selector")

while True:
	events = selector.select(timeout=2)
	for key, mask in events:
		if key.data:
			HandleMessage(key, mask)
		else:
			RegisterClient(key, mask)

# listen

# do something connection