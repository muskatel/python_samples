import socket

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 4200        # The port used by the server

user_name = input("Enter your username:  ")

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
    print("Starting chat")
    client.connect((HOST, PORT))

    user_input = ""

    while user_input != "exit":

        user_input = input("Send:  ")

        if user_input != "exit":
            client.sendall(bytes("{0}:  {1}".format(user_name, user_input), 'utf-8'))
    print("Ending chat")