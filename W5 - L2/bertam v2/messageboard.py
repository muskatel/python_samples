import socket
import selectors
import types


def CreateSocket(sock):
    conn, addr = sock.accept()  #
    print("A new user has joined the chat room")
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr)
    events = selectors.EVENT_READ
    sel.register(conn, events, data=data)


def HandleIncoming(key, mask):
    sock = key.fileobj
    data = key.data
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)
        if recv_data:
            print(repr(recv_data))
        else:
            print("A user has left the chat room")
            sel.unregister(sock)
            sock.close()


HOST = '127.0.0.1'
PORT = 4200


sel = selectors.DefaultSelector()
serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv_sock.bind((HOST, PORT))
serv_sock.listen()
print("listening on:", (HOST, PORT))

serv_sock.setblocking(False)
sel.register(serv_sock, selectors.EVENT_READ, data=None)

while True:

    events = sel.select(timeout=None)

    for key, mask in events:

        if key.data is None:
            CreateSocket(key.fileobj)
            # new connection
        else:
            HandleIncoming(key, mask)
            # existing connection
