# configure
import socket

HOST = '127.0.0.1'
PORT = 4200 # 1024 - 65535


# 'technically' the server starts here
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
	# AF_INET => IP4
	# SOCK_STREAM => TCP

	# bind
	sock.bind((HOST, PORT))
	sock.listen() # server is now listening
	print("Started server on ",HOST,PORT)

	#Add while to keep server running

	connection, address = sock.accept()
	with connection:
		print("a client conencted!")
		print("address:",address)
		while(True):
			data = connection.recv(20) # 1024 byte buffer
			if not data:
				break
			print(data)

# listen

# do something connection