import os

cwd = os.getcwd()
print("--- listdir ---")
print(os.listdir(cwd)) #could use "." instead of cwd
	

print("--- scandir ---")
entries = os.scandir(cwd)

for entry in entries:
	print(entry.name)

print("--- walk ---")
for walkentry in os.walk(cwd): 
	print(walkentry)

print("--- walk(search) ---")
for root, dirs, files in os.walk(cwd):
	print(files)
	for filename in files:
		if "hidden" in filename:
			print("FOUND!")
		if "cat.txt" in filename:
			os.rename(os.path.join(root,filename),os.path.join(root,"dog.txt"))