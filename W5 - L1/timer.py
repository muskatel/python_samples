import schedule
import time

tick = True
def ticktock():
	global tick
	if tick:
		print("tick")
	else:
		print("TOCK")
	tick = not tick

schedule.every(1).second.do(ticktock)

while True: #runs forever
	schedule.run_pending()
	time.sleep(1)


