import os
from datetime import datetime

cwd = os.getcwd()
print("--- listdir ---")

for file in os.listdir(cwd):
	file_stats = os.stat(file)
	print(file," ",datetime.fromtimestamp(file_stats.st_ctime))