def PrintHello():
	""" Prints hello to the console

	Returns
	-------
	None
	"""

	i = 0 # we need an i

	for x in range(i):
		if(i == 2):
			# this is a two
			print("two")


	print("Hello")

def GetStringHello():
	""" Gets a "hello" string

	Returns
	-------
	str
		A string value of the word "Hello"
	"""

	return "Hello"

def MakeReversed(input: str):
	""" Reverses a given string

	Parameters
	----------
	input: str
		The input string

	Returns
	-------
	str
		A input string in reverse
	"""

	return input[::-1]


def main():
	""" Main entry point

	Used for testing
	"""

	print("--- running from utility.py ---")
	PrintHello()
	print(MakeReversed("fishes"))

if __name__ == '__main__':
	main()