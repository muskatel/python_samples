import converter
from tkinter import *

currencies = ['EUR', 'USD', 'NOK', 'SEK', 'DKK', 'GBP', 'ZAR', 'PHP']

def convert():	
	conv = converter.Currency(currIn.get())
	result = conv.convert(inputAmount.get(),currOut.get())
	outputAmount.set(result)
	print("{} {} converted to {} {}".format(inputAmount.get(),currIn.get(),outputAmount.get(),currOut.get()))	

window = Tk()
window.title('Converter App')
window.geometry("500x200")

# INPUT FRAME
frameInput = Frame(window)

inputAmount = IntVar(frameInput)
inputAmount.set(420)

labelIn = Label(frameInput, text = 'Starting amount:')
labelIn.grid(row=0, column=0)

inputValue = Entry(frameInput,textvariable=inputAmount) 
inputValue.grid(row=0, column=1)

currIn = StringVar(frameInput)
currIn.set('NOK')

selectInput = OptionMenu(frameInput, currIn, *currencies)
selectInput.grid(row=0, column=2)

frameInput.pack()
# INPUT FRAME END

# OUTPUT FRAME
frameOutput = Frame(window)

outputAmount = IntVar(frameOutput)
outputAmount.set(0)

labelOut = Label(frameOutput, text = 'Output amount:')
labelOut.grid(row=0, column=0)
outputValue = Entry(frameOutput,textvariable=outputAmount) 
outputValue.grid(row=0, column=1)

currOut = StringVar(frameOutput)
currOut.set('NOK')

selectOutput = OptionMenu(frameOutput, currOut, *currencies)
selectOutput.grid(row=0, column=2)

frameOutput.pack()
# OUTPUT FRAME END

# convert button
toggleButton = Button(window,text="Convert", command=convert)
toggleButton.pack(padx = 20, pady = 20)

window.mainloop()

