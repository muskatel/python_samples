""" 
Converter Module

General purpose currency converter	
"""

import requests
import json

class Currency(object):
	"""docstring for Currency

	Currency object
	"""
	def __init__(self, currency_str: str):
		super(Currency, self).__init__()
		self.currency = currency_str.upper()

	def convert(self, input: int, target: str):
		"""Convert Function

		Parameters
		----------
		input
			int

			Input amount of currency that needs to be converted

		target
			str

			The currency we want to convert to
		"""

		target = target.upper()

		response = requests.get("https://api.exchangeratesapi.io/latest?base={}".format(self.currency))

		json_data = json.loads(response.text)
		print

		rate = json_data['rates'][target]

		output = rate * input
		return output


