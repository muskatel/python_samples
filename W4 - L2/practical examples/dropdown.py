import cgi

data = cgi.FieldStorage()

pet = data.getvalue("pet") # get the value of the thing "named" pet

print("Content-Type: text/html")
print()
print("<!DOCTYPE HTML>")
print("<html lang=”en”>")
print("<head>")
print("<meta charset=”UTF-8”>")
print("<title>pet selected</title>")
print("</head>")
print("<body>")
print("<h1>Pet selected:</h1>")
print("<p>", pet, "</p>")
print("</html>")
