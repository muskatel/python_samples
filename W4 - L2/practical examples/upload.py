import cgi, os

data = cgi.FieldStorage()

filedata = data["filename"] 
filename = os.path.basename(filedata.filename)

with open("images/"+filename, "wb") as copy:
	copy.write(filedata.file.read())

print("Content-Type: text/html")
print()
print("<!DOCTYPE HTML>")
print("<html lang=”en”>")
print("<head>")
print("<meta charset=”UTF-8”>")
print("<title>file selected</title>")
print("</head>")
print("<body>")
print("<p>",filename,"</p>")
print("<img src=\"{}\"/>".format("images/"+filename))
print("</body>")
print("</html>")
