import cgi

data = cgi.FieldStorage()

name = data.getvalue("key1")
meal = data.getvalue("meal")

print("Content-Type: text/html")
print()
print("<!DOCTYPE HTML>")
print("<html lang=”en”>")
print("<head>")
print("<meta charset=”UTF-8”>")
print("<title>Radio Buttons Target</title>")
print("</head>")
print("<body>")
print("<h1>Radio Buttons Target</h1>")
print("<p>User: ", name, "</p>")
print("<p>Meal: ", meal, "</p>")
print("<a href='index.html'>back to Index</a>")
print("</body>")
print("</html>")

