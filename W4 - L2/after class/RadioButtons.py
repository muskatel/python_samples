print("Content-Type: text/html")
print()
print("<!DOCTYPE HTML>")
print("<html>")
print("<head>")
print("<meta charset='utf-8'>")
print("<title>Craig's Test</title>")
print("</head>")
print("<body>")
print("<h1>Hello PRG class!</h1>")
print("<h2>Please enter your name below:</h2>")
print("<form method='POST' action='RadioButtonsTarget.py'>")
print("<input type='text' name='key1'>")
print("<p>Please choose food:</p>")
print("<input type='radio' id='soup' value='Soup' name='meal' checked>Soup!</input>")
print("<input type='radio' id='burger' value='Burger' name='meal' unchecked>Burger!</input>")
print("<input type='radio' id='pizza' value='Pizza' name='meal' unchecked>Pizza!</input>")
print("<input type='submit' value='click me!'>")
print("</form>")
print("<a href='index.html'>back to Index</a>")
print("</body>")
print("</html>")