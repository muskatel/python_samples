import cgi

data = cgi.FieldStorage()

pet_id = data.getvalue("pet_id")

print("Content-Type: text/html")
print()
print("<!DOCTYPE HTML>")
print("<html lang=”en”>")
print("<head>")
print("<meta charset=”UTF-8”>")
print("<title>Pet details</title>")
print("</head>")
print("<body>")

if(pet_id == "1"):
	print("<h1>Pet 1: Fluffy</h1>")
elif (pet_id == "2"):
	print("<h1>Pet 2: Spot</h1>")
else:
	print("<h1>No pet found with id:",pet_id,"</h1>")


print("</body>")
print("</html>")