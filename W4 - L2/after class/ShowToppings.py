import cgi

data = cgi.FieldStorage()

toppings_python = data.getvalue("toppings")

print("Content-Type: text/html")
print()
print("<!DOCTYPE HTML>")
print("<html lang=”en”>")
print("<head>")
print("<meta charset=”UTF-8”>")
print("<title>Toppings</title>")
print("</head>")
print("<body>")
print("<h1>Toppings:</h1>")
for topping in toppings_python:
	print("<p> ", topping, "</p>")
	if(topping == "pineapples"):
		print("<img src='really.gif'>")
print("</body>")
print("</html>")
