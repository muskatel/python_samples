from tkinter import *
import tkinter.messagebox as box


window = Tk()
window.title('Pizza Menu')
window.geometry("500x547")

# window colours, for some ducking reason
# with images!
window.configure(bg = 'green')

pizzaImage = PhotoImage(file= "pizza.gif")
can = Canvas(window, width=250,height=250,bg='red')

can.create_image((100,100),image= pizzaImage)
can.pack(side= TOP)
# listbox with pizzas

fPizzas = Frame(window)
fPizzas.configure(bg = 'yellow')

def select():
	print("hello")

listbox = Listbox(fPizzas)
listbox.insert(1, "DEN ENKLE")
listbox.insert(2, "MEKSIKANEREN")
listbox.insert(3, "BIFFEN")

listbox.pack()

def updateIngredients(data):
	global labelIngre
	if data == "DEN ENKLE":
		labelIngre.configure(text = "Ost, tomatsaus")
	elif data == "MEKSIKANEREN":
		labelIngre.configure(text = "Ost, tomatsaus, marinert kylling, marinert biff, nachoschips, hvitløk, mais og chili")
	elif data == "BIFFEN":
		labelIngre.configure(text = "Ost, tomatsaus, marinert biff i strimler, bacon og hvitløk")
	else:
		labelIngre.configure(text = "")

def callback(event):
    selection = event.widget.curselection()
    if selection:
        index = selection[0]
        data = event.widget.get(index)
        label.configure(text=data)
        updateIngredients(data)
    else:
        label.configure(text="")

listbox.bind("<<ListboxSelect>>", callback)
fPizzas.pack(side = LEFT)
# labels with ingredients

fIngre = Frame(window)
fIngre.configure(bg = 'pink')

label = Label(fIngre, text = "ingredients:", fg = 'red')
label.pack()

labelIngre = Label(fIngre, text = "tomat", fg = 'red')
labelIngre.pack()

fIngre.pack()
# radio buttons for thick / thin

base = StringVar()

fBase = Frame(window)
fIngre.configure(bg = 'orange')

radioThick = Radiobutton(fBase, text = "Thick", variable = base, value = "thick")
radioThin = Radiobutton(fBase, text = "Thin", variable = base, value = "thin")

radioThick.select()

radioThick.pack()
radioThin.pack()

fBase.pack()
# checkboxes for extra toppings

fExtra = Frame(window)

mage = BooleanVar()
warrior = BooleanVar()
rogue = BooleanVar()
hunter = BooleanVar()
paladin = BooleanVar()
priest = BooleanVar()

def changeMage():
    labelmage.configure(text=mage.get())
def changeWarrior():
    labelwarrior.configure(text=warrior.get())
def changeRogue():
    labelrogue.configure(text=rogue.get())
def changeHunter():
    labelhunter.configure(text=hunter.get())
def changePaladin():
    labelpaladin.configure(text=paladin.get())
def changePriest():
    labelpriest.configure(text=priest.get())

mageButton = Checkbutton(fExtra,text="mage",variable = mage,onvalue=True, offvalue=False, command=changeMage)
warriorButton = Checkbutton(fExtra,text="warrior",variable = warrior,onvalue=True, offvalue=False, command=changeWarrior)
rogueButton = Checkbutton(fExtra,text="rogue",variable = rogue,onvalue=True, offvalue=False, command=changeRogue)
hunterButton = Checkbutton(fExtra,text="hunter",variable = hunter,onvalue=True, offvalue=False, command=changeHunter)
paladinButton = Checkbutton(fExtra,text="paladin",variable = paladin,onvalue=True, offvalue=False, command=changePaladin)
priestButton = Checkbutton(fExtra,text="priest",variable = priest,onvalue=True, offvalue=False, command=changePriest)

mageButton.grid(row=0,column=0)
warriorButton.grid(row=1,column=0)
rogueButton.grid(row=2,column=0)
hunterButton.grid(row=3,column=0)
paladinButton.grid(row=4,column=0)
priestButton.grid(row=5,column=0)

labelmage = Label(fExtra,text=mage.get())
labelwarrior = Label(fExtra,text=warrior.get())
labelrogue = Label(fExtra,text=rogue.get())
labelhunter = Label(fExtra,text=hunter.get())
labelpaladin = Label(fExtra,text=paladin.get())
labelpriest = Label(fExtra,text=priest.get())

labelmage.grid(row=0,column=1)
labelwarrior.grid(row=1,column=1)
labelrogue.grid(row=2,column=1)
labelhunter.grid(row=3,column=1)
labelpaladin.grid(row=4,column=1)
labelpriest.grid(row=5,column=1)

fExtra.pack()

# Order button
def order():
	if listbox.curselection() != ():
		print("Pizza time!")
		print(listbox.get(listbox.curselection()[0]))
		print(base.get())

order = Button(window, command = order, text = "order dat pizza")
order.pack()



window.mainloop()