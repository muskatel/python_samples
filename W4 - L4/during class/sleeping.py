from tkinter import *
import tkinter.messagebox as box

window = Tk()
window.title('Checkboxes')
window.geometry("500x200")

mage = BooleanVar()
warrior = BooleanVar()
rogue = BooleanVar()
hunter = BooleanVar()
paladin = BooleanVar()
priest = BooleanVar()

def changeMage():
    labelmage.configure(text=mage.get())
def changeWarrior():
    labelwarrior.configure(text=warrior.get())
def changeRogue():
    labelrogue.configure(text=rogue.get())
def changeHunter():
    labelhunter.configure(text=hunter.get())
def changePaladin():
    labelpaladin.configure(text=paladin.get())
def changePriest():
    labelpriest.configure(text=priest.get())

mageButton = Checkbutton(window,text="mage",variable = mage,onvalue=True, offvalue=False, command=changeMage)
warriorButton = Checkbutton(window,text="warrior",variable = warrior,onvalue=True, offvalue=False, command=changeWarrior)
rogueButton = Checkbutton(window,text="rogue",variable = rogue,onvalue=True, offvalue=False, command=changeRogue)
hunterButton = Checkbutton(window,text="hunter",variable = hunter,onvalue=True, offvalue=False, command=changeHunter)
paladinButton = Checkbutton(window,text="paladin",variable = paladin,onvalue=True, offvalue=False, command=changePaladin)
priestButton = Checkbutton(window,text="priest",variable = priest,onvalue=True, offvalue=False, command=changePriest)

mageButton.grid(row=0,column=0)
warriorButton.grid(row=1,column=0)
rogueButton.grid(row=2,column=0)
hunterButton.grid(row=3,column=0)
paladinButton.grid(row=4,column=0)
priestButton.grid(row=5,column=0)

labelmage = Label(window,text=mage.get())
labelwarrior = Label(window,text=warrior.get())
labelrogue = Label(window,text=rogue.get())
labelhunter = Label(window,text=hunter.get())
labelpaladin = Label(window,text=paladin.get())
labelpriest = Label(window,text=priest.get())

labelmage.grid(row=0,column=1)
labelwarrior.grid(row=1,column=1)
labelrogue.grid(row=2,column=1)
labelhunter.grid(row=3,column=1)
labelpaladin.grid(row=4,column=1)
labelpriest.grid(row=5,column=1)

window.mainloop()