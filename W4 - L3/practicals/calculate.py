# Create a script called calculate.py. 
# This script should generate a window 
# containing two entry boxes and 3 buttons. 
# The entry boxes should be used to capture 
# numeric values from the user. The buttons 
# should be called Add, Subtract and Multiply. 
# If the user clicks any of the 3 buttons, the 
# program should perform the calculation 
# specified by the button (e.g. addition for add) 
# on the 2 values entered into the entry boxes. 
# The result of the calculation should be displayed 
# using an information messagebox.

from tkinter import *

window = Tk()
window.title('Calculate')
window.geometry("400x400")

frame1 = Frame(window)
frame2 = Frame(window)
frame3 = Frame(window)

label1 = Label(frame1, text="Value 1: ")
label2 = Label(frame2, text="Value 2: ")
label3 = Label(frame3, text="Result: ")

result = Label(frame3, text = "nothing")

entry1 = Entry(frame1)
entry2 = Entry(frame2)


label1.grid(row=0, column=0)
entry1.grid(row=0, column=1)

label2.grid(row=0, column=0)
entry2.grid(row=0, column=1)

label3.grid(row=0, column=0)
result.grid(row=0, column=1)

frame1.grid(row=0, column=0)
frame2.grid(row=1, column=0)
frame3.grid(row=2, column=0)

def add():
	# get first value
	global entry1
	val1 = int(entry1.get())
	# get second value
	global entry2
	val2 = int(entry2.get())

	# add them
	val3 = val1 + val2

	# display result
	global result
	result.configure(text=val3)

def subtract():
	# get first value
	global entry1
	val1 = int(entry1.get())
	# get second value
	global entry2
	val2 = int(entry2.get())

	# subtract them
	val3 = val1 - val2

	# display result
	global result
	result.configure(text=val3)

def multiply():
		# get first value
	global entry1
	val1 = int(entry1.get())
	# get second value
	global entry2
	val2 = int(entry2.get())

	# subtract them
	val3 = val1 * val2

	# display result
	global result
	result.configure(text=val3)

def divide():
	return 0

btnAdd=Button(window, text="Add", command=add)
btnSubtract=Button(window, text="Subtract", command=subtract)
btnMultiply=Button(window, text="Multiply", command=multiply)
# add btnDivide

btnAdd.grid(row=0,column=2)
btnSubtract.grid(row=1,column=2)
btnMultiply.grid(row=2,column=2)
# place button on grid

window.mainloop()



























