# 1.
# Create a script called displayme.py. 
# This script should create a window with 2 labels. 
# Use these 2 labels to display your name and surname (in one label) 
# and a description of yourself in the other.

# Copyright 2021, Craig Marais ([@muskatel](https://gitlab.com/muskatel))

from tkinter import *

window = Tk()
window.title('About me!')

label1 = Label(window, text = 'Craigsworth Williamsmyth Maraisington')
label1.pack(padx = 50, pady = 50)

label2 = Label(window, text = 'Owns too many keyboards.')
label2.pack(padx = 50, pady = 50)

window.mainloop()