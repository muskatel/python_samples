from tkinter import *
import tkinter.messagebox as box

window = Tk()
window.title('Profile Generator')
window.geometry("500x200")

def question1():
	msg = box.askyesno("Question 1","question 1")
	if msg:
		#if answered yes/true
		# question2
		question2()
	else:
		#if answered no/false
		#question3
		question3()

def question2():
	msg = box.askyesno("Question 2","question 2")
	if msg:
		#if answered yes/true
		print("Q2 - T")
	else:
		#if answered no/false
		print("Q2 - F")

def question3():
	msg = box.askyesno("Question 3","question 3")
	if msg:
		#if answered yes/true
		print("Q3 - T")
	else:
		#if answered no/false
		print("Q3 - F")


btnStart=Button(window, text = "Create new Profile", command = question1)
btnStart.pack(padx=20,pady=20)

window.mainloop()