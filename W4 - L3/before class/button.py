from tkinter import *

window = Tk()
window.title('button window')

def toggleColour():
	if window.cget('bg') == 'yellow':
		window.configure(bg='gray')
	else:
		window.configure(bg='yellow')

btn_Exit=Button(window, text='Close', command=exit)
btn_ToggleColour=Button(window, text='Toggle', command=toggleColour)


btn_Exit.pack(padx = 50, pady = 50)
btn_ToggleColour.pack(padx = 200, pady = 50)

window.mainloop()