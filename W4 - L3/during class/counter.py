from tkinter import *

window = Tk()
window.title('button window')

num = 0

def count():
	global num
	global label

	num=num+1					# increase num value
	label.configure(text = "Count: {}".format(num)) # set the label to the new num value


label = Label(window, text = "Count: {}".format(num))
label.pack(padx = 200, pady = 50)

btn_ToggleColour=Button(window, text='Toggle', command=count) #run count() when clicked
btn_ToggleColour.pack(padx = 200, pady = 50)

window.mainloop()