from tkinter import *

window = Tk()
window.title('a new window')
window.geometry("500x200")

label = Label(window, text = 'Enter you name:')
label.grid(row=0, column=0)

frame = Frame(window)
name_entry = Entry(frame) 

name_entry.pack()
frame.grid(row=2, column=0)

def printName():
	global name_entry
	print(name_entry.get())

btnEntry=Button(window, text='Print Name to Console', command=printName)
btnEntry.grid(row=4,column=0)

window.mainloop()
