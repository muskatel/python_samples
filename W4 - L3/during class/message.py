from tkinter import *
import tkinter.messagebox as box

window = Tk()
window.title('mesage maker')

# display a message
def message_hammer():
	msg = box.showwarning("Stop","Hammer time!")

btn_Hammer=Button(window, text='Hammer', command=message_hammer)
btn_Hammer.pack(padx = 200, pady = 50)

def message_info():
	msg = box.showinfo("Info","Today is Thursday!")

btn_Info=Button(window, text='Thursday', command=message_info)
btn_Info.pack(padx = 200, pady = 50)

def message_ask():
	msg = box.askyesno("Question?","How much wood could a wood chuck chuck if a wood chuck could chuck wood?")
	if not msg:
		box.showwarning("Stop","OH NO!")

btn_Ask=Button(window, text='QUESTION TIME', command=message_ask)
btn_Ask.pack(padx = 200, pady = 50)

window.mainloop()