import math

A0 = 1.0 # 1m^2

def sides(area):
	width2 = area / 1.4142 
	width = math.sqrt(width2)
	length = width * 1.4142 # golden ratio
	return [length, width]

for n in range(5):
	length, width = sides(A0/2 ** n)
	print("A{0}: {1:.2f}m x {2:.2f}m".format(n,length,width))