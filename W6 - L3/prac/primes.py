"""Prime Finder

Purpose
-------

A prime number is any positive integer that only has 2 factors, 
namely 1 and itself, i.e. it’s a positive integer that is not 
the product of two smaller positive integers.  
The first 10 prime numbers are as follows:
2, 3, 5, 7, 11, 13, 17, 19, 23, 29

For this exercise you are required to demonstrate how to find 
the nth prime number, e.g. if you request the 3rd prime number 
your code should output the value 5, if you request the 10th 
prime number your code should output the value 29.  Your code 
should be able to output any prime number requested, e.g. the 
100th or the 34556th.  Be wary of testing your code with too 
high a prime number since it may take some time to compute.
You have learned about the functional, procedural and 
object-oriented programming styles.  For this exercise you 
are required to write scripts to demonstrate how to calculate 
the nth prime number using at least two of the programming styles.  
If you have the time and feel up to the challenge, try writing 
scripts for all three programming styles.

Functions
---------

CheckIfPrime() 		 # 
FindPrime()			 # imperative 
FindPrimeRecursive() # functional

"""

import math

primes = []		# all the primes we have found thus far

def CheckIfPrime(num: int):
	""" Check if Prime

	Parameters
	----------
	num: int
		The provided number, that will be evaluated for "primeness"

	Returns
	-------
	bool
		Return True is the provided number is prime, otherwise returns False
	"""

	# 1 is always not prime, so skip it
	if(num == 1):
		return False

	# compare current number with numbers less than it
	for div in primes:
		if(div < math.floor(math.sqrt(num)+1)):			# for efficiency
			if(num % div == 0): 						# check if num is divisible by div
				#print("{} was divisible by {}".format(num, div))
				return False

	# if it's not divisible by any numbers less that itself, then it is prime
	#print("we did {} checks".format(checks))
	return True



def FindPrime(primeNumber: int):
	""" Check if Prime

	Parameters
	----------
	primeNumber: int
		Will search this no. prime

	ie 7 will search for the 7th prime

	Returns
	-------
	int
		Return the value for the requested prime
	"""

	currentNumber = 1 							# the current number we are checking for primeness
	while len(primes) < primeNumber:			# check if we have found enough primes
		if CheckIfPrime(currentNumber):			# check if the current number is prime
			if currentNumber not in primes:
				primes.append(currentNumber)	# add this to found primes
		currentNumber +=1
	return primes[primeNumber-1]				# return the requested prime

def main():
	user_input = int(input("Please enter a number:"))
	print("The {}th prime is {}".format(user_input, FindPrime(user_input)))

if __name__ == '__main__':
	main()

#test_nums = [9999,10000]
#for num in test_nums:
	#print("The {}th prime is {}".format(num, FindPrime(num)))

#test_nums = [7,18,27,37,3,42,108, 109, 541]
#test_nums = [7919]

#test our function with test numbers
#for num in test_nums:
#	print("Checking {} for primeness ...".format(num))
#	print("Result:", CheckIfPrime(num))
#	print("")
