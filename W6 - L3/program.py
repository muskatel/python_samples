import copy 
the_list = [1,2,3,4,5,6,7,8,9,10]

print(the_list)

# normal imperative process

sum = 0

for x in the_list:
	sum += x

print("sum: ",sum)



# reduced list to a single result
# functional approach
def add(num_list):
	if(len(num_list) > 0):
		return num_list.pop(0) + add(num_list)
	else:
		return 0

# filter list based on value
def even(num_list):
	even_list = []
	for num in the_list:
		if num % 2 == 0:
			even_list.append(num)
	return even_list

# map each value into a new value
def double(num_list):
	doub_list = []
	for num in the_list:
		doub_list.append(num *2)
	return doub_list

print(double(the_list))

new_list = copy.copy(the_list)
print("before: ", new_list)
print("recursive sum: ", add(new_list)) # modifies original
print("after: ", new_list)




