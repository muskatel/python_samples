# Classes for
- StockItems
- StockTracker

# A Main.py script for
- displaying the CLI menu,
- accessing the StockTracker
- storing and retrieving the StockTracker
- performing threading
- running a server

# A Client.py script for
- creating a GUI-based interface
- capturing a StockItem’s code, description and amount
- acting as a client to the server
- displaying a messagebox

# Your code needs to be fully commented and include docstrings.