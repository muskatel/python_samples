print("StockTracker.py loaded!")
# StockTracker.py

# Craig Marais, 2021

# Must contain:
# collection of [StockItem] 		✓ 
# add [StockItem] method    		✓ 
# find [StockItem key] by [code]	✓ 
# update StockItem amount by [code]	✓
# find [StockItem] by [key]			✗

import StockItem

WarehouseStock = [] # this is out big boy collection of things


def AddStockItem(stockItem):
	global WarehouseStock
	WarehouseStock.append(stockItem)

def FindKeyByCode(code):
	global WarehouseStock

	for item in WarehouseStock:
		if code == item.itemcode:
			return WarehouseStock.index(item)
	return -1 # retrun -1 if not found

def UpdateByCode(code, amount):
	global WarehouseStock

	currentItem = FindKeyByCode(code)
	if currentItem >= 0:
		WarehouseStock[currentItem].updateAmount(amount)
	else:
		print("Item not found")

def PrintStock():
	print(" --- WAREHOUSE STOCK ---")
	for item in WarehouseStock:
		print(item.itemcode,item.description,item.stockamount)
	print(" -----------------------")