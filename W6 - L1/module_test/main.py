import utility
import Pet
import stuff.printer

def main():
	print("--- Running from main ---")
	utility.PrintHello()

	myDog = Pet.Dog("Spot")
	myDog.Bark()

	myCat = Pet.Cat("Mr Flufflekins")
	myCat.Meow()

if __name__ == '__main__':
	main()