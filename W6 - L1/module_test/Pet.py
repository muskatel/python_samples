def thing():
	print("a thing")


class Dog(object):
	"""docstring for Dog"""
	def __init__(self, name):
		super(Dog, self).__init__()
		self.name = name

	def Bark(self):
		print("{} says woof!".format(self.name))

class Cat(object):
	"""docstring for Cat"""
	def __init__(self, name):
		super(Cat, self).__init__()
		self.name = name

	def Meow(self):
		print("{} says meow!".format(self.name))
		