from tkinter import *
import tkinter.messagebox as box

window = Tk()
window.title('Button Toggle')
window.geometry("500x200")

visible = True

message = "Hello, Class!"

def toggleAThing():
	print("clicked.")
	global visible
	visible = not visible
	if visible:
		labelThing.configure(text=message)
	else:
		labelThing.configure(text="")

toggleButton = Button(window,text="Toggle!", command=toggleAThing)
toggleButton.pack(padx = 20, pady = 20)

labelThing = Label(window,text=message)
labelThing.pack(padx = 20, pady = 20)

window.mainloop()