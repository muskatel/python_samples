import numpy as np
import threading
import time

import multiprocessing

nums = []
average = 0;
smallest = 999
aboves = []

#Load()
def LoadValues():
    global nums
    nums = np.random.randint(1,100,2000000)


def FindAverage():
    global nums
    global average
    count = 0
    total = 0
    for num in nums:
        count += 1
        total += num
    if count > 0:
        average = total/count
    else:
        average = 0

def FindSmallest():
    global nums
    global smallest
    for num in nums:
        if num < smallest:
            smallest = num

def AboveAverage():
    global aboves
    for num in nums:
        if num > average:
            aboves.append(num)

threadAverage = threading.Thread(target=FindAverage,args=())
threadSmallest = threading.Thread(target=FindSmallest,args=())
threadAboves = threading.Thread(target=AboveAverage,args=())

#Main function call
if __name__ == '__main__':
    jobs = {
        "load": multiprocessing.Process(target=LoadValues,args=())
    }

    jobs["load"].start()
    jobs["load"].join()


    threadAverage.start()
    threadSmallest.start()

    print("Main thread joining AVERAGE thread")
    threadAverage.join() # main thread waits for average
    print("done ...")

    threadAboves.start()

    print("Main thread joining ABOVES thread")
    threadAboves.join() # main thread waits for aboves
    print("done ...")

    print("Numbers (count):", len(nums))
    print("\nAverage:",average)
    print("\nSmallest:",smallest)
    print("\nAbove Average (count):", len(aboves))
   # aboves = AboveAverage(average,nums)
   # print("\nAbove Average (count):", len(aboves))

