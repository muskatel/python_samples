#poollock
import sys
import threading


class Arithmetic():
	def __init__(self):
		self.lock = threading.Lock()
		self.total = 0


	def add(self, num1, num2):
		self.lock.acquire()
		change = (num1 + num2)
		newTotal = self.total + change
		print("{} + {} = {} + {} = {}".format(num1,num2,change,self.total, newTotal))
		self.total = newTotal
		self.lock.release()

	def sub(self, num1, num2):
		self.lock.acquire()
		change = (num1 - num2)
		newTotal = self.total + change
		print("{} - {} = {} + {} = {}".format(num1,num2,change,self.total, newTotal))
		self.total = newTotal
		self.lock.release()

	def mul(self, num1, num2):
		self.lock.acquire()
		change = (num1 * num2)
		newTotal = self.total + change
		print("{} * {} = {} + {} = {}".format(num1,num2,change,self.total, newTotal))
		self.total = newTotal
		self.lock.release()


def main(argv):
	print("-- Part 2 of Prac ---")

	tester = Arithmetic()
	tester.add(35,7)
	tester.sub(35,7)
	tester.mul(35,7)


if __name__ == "__main__":
	main(sys.argv)