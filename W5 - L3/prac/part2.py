import poollock
import sys

from concurrent.futures import ThreadPoolExecutor
import threading
import random

import time



def main(argv):
	calc = poollock.Arithmetic()
	print("-- Running in Threads! ---")

	executor = ThreadPoolExecutor(max_workers = 1000)

	for threadNum in range(100000):
		# get a function
		opNum = random.randrange(3)
		if opNum == 0:
			op = calc.add
		elif opNum == 1:
			op = calc.sub
		elif opNum ==2:
			op = calc.mul
		else:
			#HALT AND CATCH FIRE
			return 0
		# get 2 number 
		num1 = random.randrange(10)
		num2 = random.randrange(10)
		# assign
		task = executor.submit(op,num1,num2)
		#time.sleep(0.01)



if __name__ == "__main__":
	main(sys.argv)