import threading
import time

def printLines():
	while True:
		print('-')

def printCount(num):
	for x in range(num):
		print(x)

lineThread = threading.Thread(target=printLines,args=())
printThread = threading.Thread(target=printCount, args=(100,))

lineThread.daemon = True # stops the thread when main thread ends
printThread.daemon = True

lineThread.start()
printThread.start()

#while printThread.is_alive():
#	time.sleep(0.0001)

print("stopping ... ")



#Main  
#	line,print  
#	main waits until print is done, main terminates  
#	print ends
#	main kills line