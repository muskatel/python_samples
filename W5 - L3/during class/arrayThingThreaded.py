import numpy as np
import threading
import time

nums = []
average = 0;
smallest = 999
aboves = []

#Load()
def LoadValues():
    global nums
    nums = np.random.randint(1,100,20000)

def FindAverage():
    global nums
    global average
    count = 0
    total = 0
    for num in nums:
        count += 1
        total += num
    if count > 0:
        average = total/count
    else:
        average = 0

def FindSmallest():
    global nums
    global smallest
    for num in nums:
        if num < smallest:
            smallest = num

def AboveAverage():
    global aboves
    for num in nums:
        if num > average:
            aboves.append(num)

#Main function call
if __name__ == '__main__':
    threadLoad = threading.Thread(target=LoadValues,args=())
    threadAverage = threading.Thread(target=FindAverage,args=())
    threadSmallest = threading.Thread(target=FindSmallest,args=())
    threadAboves = threading.Thread(target=AboveAverage,args=())

    threadLoad.start()

    while threadLoad.is_alive():
        time.sleep(0.0001) # sleep main thread
    threadAverage.start()
    threadSmallest.start()

    while threadAverage.is_alive():
        time.sleep(0.0001) # sleep main thread

    threadAboves.start()

    while threadAboves.is_alive():
        time.sleep(0.0001) # sleep main thread

    print("Numbers (count):", len(nums))
    print("\nAverage:",average)
    print("\nSmallest:",smallest)
    print("\nAbove Average (count):", len(aboves))
   # aboves = AboveAverage(average,nums)
   # print("\nAbove Average (count):", len(aboves))

