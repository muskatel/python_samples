import numpy as np

#Load()
def LoadValues():
    return np.random.randint(1,100,200000)

def FindAverage(nums):
    count = 0
    total = 0
    for num in nums:
        count += 1
        total += num
    if count > 0:
        return total/count
    else:
        return 0

def FindSmallest(nums):
    smallest = 999
    for num in nums:
        if num < smallest:
            smallest = num
    return smallest

def AboveAverage(average,nums):
    aboves = []
    for num in nums:
        if num > average:
            aboves.append(num)
    return aboves

if __name__ == '__main__':
    nums = LoadValues()
    average = FindAverage(nums)
    smallest = FindSmallest(nums)
    aboves = AboveAverage(average,nums)

    print("Numbers (count):", len(nums))
    print("\nAverage:",average)
    print("\nSmallest:",smallest)
    print("\nAbove Average (count):", len(aboves))

