from pysnmp.hlapi import *

errorIndication, errorStatus, errorIndex, varBinds = next(
	getCmd(SnmpEngine(),
		CommunityData("public"),
		UdpTransportTarget(('demo.snmplabs.com',161)),
		ContextData(),
		ObjectType(ObjectIdentity("1.3.6.1.2.1.1.1.0"))
	)
)

if errorIndication:
	print(errorIndication)
elif errorStatus:
	print(errorStatus,errorIndex,varBinds[errorIndex-1])
else:
	for varBind in varBinds:
		print(varBind[0],varBind[1])
		